#-*-coding:utf8;-*-
import random
import time
from os import system

# Gioco con i numeri sommandoli, sottraendoli, moltiplicandoli e dividendoli
# Funzione che torna i numeri random generati
def rand_num(qta, da, a):
    num = []
    for x in range(qta):
        num.append(random.randint(da,a))
    return num
    
# Funzione che controlla se hai vinto o no
def risultato(user_ris, ris):
    if(user_ris == ris):
        print("Bravo hai risposto correttamente!")
    else:
        print("Risposta sbagliata!!!\nDai riprovaci!")
        print("Il risultato è " + str(ris))
    
    time.sleep(3)
    
# Funzione gioca
def gioca(qta, da, a, op):
    num = rand_num(qta, da, a)
    if( op == 1):
        ris = 0
        for i, y in enumerate(num):
            print(str(y), end = '')
            if i != len(num) - 1:
                print(" + ", end = '')
            ris += y
    elif( op == 2):
        ris = 0
        for i, y in enumerate(num, 1):
            #if( i == 1):
            #    ris = y
            #print(str(y), end = '')
            #if i != len(num) - 1:
            #    print(" - ", end = '')
            #ris -= y
            print("i: " + str(i) + " y: " + str(y))
    elif( op == 3 ):
         num = rand_num(qta, da, a)
         ris = num[0] > num[1]
         print(str(num[0]) + " > " + str(num[1]))
         print("1 - SI, 0 - NO")
                     
    user_ris = int(input("\nDimmi il risultato: "))
    risultato(user_ris, ris)    

while True:
    system('clear')
    # Chiedi che operazioni vuoi fare
    print('''Operazioni:
        1. Addizione
        2. Sottrazione
        3. Mggiore o minore
        4. Moltiplicazione
        5. Divisione
        6. ESCI
    ''')
    try:
        operazione = int(input("Che operazione scegli? "))
    except ValueError:
        continue
    if operazione == 1:
        # Chiedi quanti numeri si vuole usare
        while True:
            try:
                qta = int(input("Con quanti numeri vuoi giocare (Almeno 2)? "))
                if qta >= 2:
                    break
            except ValueError:
                continue
    elif operazione == 3:
        qta = 2
    elif operazione == 6:
        exit()
    else:
        print("Ancora non abbiamo preparato questa funzione!")
        print("Prova un'altra tipo di operazione")
        time.sleep(3)
        continue
        
    # Chiedi il range dei numeri
    while True:
        try:
            da = int(input("I numeri iniziano da: "))
            break
        except ValueError:
            print("Inserire un valore valido!")
            continue
        
    while True:
        try:
            a = int(input("I numeri arriveranno a: "))
            if a >= da:
                break
        except ValueError:
            print("Inserire un valore valido!")
            continue
        
    
    gioca(qta, da, a, operazione)